import os
import matplotlib.pyplot as plt
import matplotlib.colors as mcolors
import matplotlib.font_manager as font_manager

import datetime
from pdb import set_trace as bp
import numpy as np

def main():
    # font
    font_dirs = ['C:\\Windows\\Fonts',]
    font_files = font_manager.findSystemFonts(fontpaths=font_dirs)
    font_list = font_manager.createFontList(font_files)
    font_manager.fontManager.ttflist.extend(font_list)

    fig, axs = plt.subplots(2,1,figsize=(9,16), subplot_kw={'polar':True})
    fig.set_facecolor('dimgray')
    color_list = list(mcolors.CSS4_COLORS.keys())

    with open(os.path.join(os.path.dirname(__file__),'play_time.txt'), 'r') as f:
        x_axis = []
        count = 1
        prev_day = None
        color_picker = None
        while True:
            _line = f.readline()
            if _line == '':
                break
            else:
                _line = _line.replace('\n','').split(' ')
                # start 21-Mar-07 23:21:38
                _start = _line[0] == 'start'
                if _start:
                    _time_start = datetime.datetime.strptime(' '.join(_line[1:]), '%y-%b-%d %H:%M:%S')
                    x_axis.append(_time_start.strftime('%y-%b-%d'))
                    # read end pair
                    _line = f.readline()
                    _line = _line.replace('\n','').split(' ')
                    _time_end = datetime.datetime.strptime(' '.join(_line[1:]), '%y-%b-%d %H:%M:%S')
                    if prev_day != _time_start.day:
                        prev_day = _time_start.day
                        color_picker = np.random.randint(len(color_list))
                    color = color_list[color_picker]
                    # plot
                    if _time_start.hour < 12 and _time_start.hour > 6 : _time_start = _time_start - datetime.timedelta(hours=2); _time_end = _time_end - datetime.timedelta(hours=2)

                    if _time_start.hour < 12:
                        axs[0].plot([np.deg2rad((_time_start.hour+_time_start.minute/60.0)*30)]*2, [0, 1], c = color)
                        axs[0].plot([np.deg2rad((_time_end.hour+_time_end.minute/60.0)*30)]*2, [0, 1], c = color)
                        theta_to_fill = [np.deg2rad((_time_start.hour+_time_start.minute/60.0)*30), np.deg2rad((_time_end.hour+_time_end.minute/60.0)*30)]
                        axs[0].fill_between(theta_to_fill, 0, 1.4, color=color, alpha=0.1)
                    else:
                        axs[1].plot([np.deg2rad((_time_start.hour+_time_start.minute/60.0)*30)]*2, [0, 1],  c = color)
                        axs[1].plot([np.deg2rad((_time_end.hour+_time_end.minute/60.0)*30)]*2, [0, 1],  c = color)
                        theta_to_fill = [np.deg2rad((_time_start.hour+_time_start.minute/60.0)*30), np.deg2rad((_time_end.hour+_time_end.minute/60.0)*30)]
                        axs[1].fill_between(theta_to_fill, 0, 1.4, color=color, alpha=0.1)
                    count += 1
    
    plt.rcParams['axes.titlepad'] = 10  # pad is in points...
    for idx, ax in enumerate(axs):
        ax.set_theta_zero_location('N')
        grids = np.arange(0,360,360/12)
        labels = [12] + [str(x) for x in range(1,12)]
        ax.set_thetagrids(grids, labels, fontsize=14, fontname='Calibri')
        ax.set_theta_direction(-1)
        ax.set_rticks([])
        ax.set_title('Game session, '+('AM' if idx==0 else 'PM'), fontname='Calibri', fontsize=14)
        ax.title.set_color('whitesmoke')
        ax.set_rmax(1)
        ax.set_facecolor('dimgray')
        ax.tick_params(axis='x', colors='whitesmoke')
        ax.grid(linewidth=0)
        for spine in ax.spines:
            ax.spines[spine].set_color('whitesmoke')

    plt.savefig('{}.png'.format(datetime.datetime.now().strftime('%Y-%m-%d')), facecolor=fig.get_facecolor())
    plt.show()
        

if __name__ == '__main__':
    main()