from pdb import set_trace as bp
import psutil
import datetime
import os
import time

def main():
    play_state = False
    time_open = None
    while True:
        all_process = [process.name() for process in psutil.process_iter()]
        genshin = [True if 'Genshin' in x else False for x in all_process]
        genshin = sum(genshin)

        if genshin and not play_state:
            file_exist = os.path.isfile(os.path.join(os.path.dirname(__file__), 'play_time.txt'))
            with open(os.path.join(os.path.dirname(__file__), 'play_time.txt'), 'a' if file_exist else 'w') as f:
                print('logging start time')
                f.write('{} {}\n'.format('start', datetime.datetime.now().strftime('%y-%b-%d %H:%M:%S')))
            play_state = True
            time_open = datetime.datetime.now()
        elif not genshin and play_state:
            file_exist = os.path.isfile(os.path.join(os.path.dirname(__file__), 'play_time.txt'))
            with open(os.path.join(os.path.dirname(__file__), 'play_time.txt'), 'a' if file_exist else 'w') as f:
                print('logging end time')
                f.write('{} {}\n'.format('end', datetime.datetime.now().strftime('%y-%b-%d %H:%M:%S')))
            play_state = False
        
        print('Genshin found:{} - Play state:{}'.format(genshin, play_state))
        if play_state:
            print('playtime: {}'.format(datetime.datetime.now() - time_open))
        time.sleep(30)

if __name__ == '__main__':
    print('Running play time logger')
    main()