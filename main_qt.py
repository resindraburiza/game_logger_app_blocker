from typing import List
from PyQt5 import QtCore
from PyQt5.QtCore import QTimer, Qt, QObject, QThread, pyqtSignal, QEvent
from PyQt5.QtWidgets import QApplication, QMainWindow, QSystemTrayIcon, QCheckBox
from PyQt5.QtGui import QCloseEvent, QIcon
import sys
from master_ui import Ui_MainWindow
import os
import datetime
import psutil

from pdb import set_trace as bp

def strfdelta(tdelta: datetime.timedelta):
    d = {"days": tdelta.days}
    d["hours"], rem = divmod(tdelta.seconds, 3600)
    d["minutes"], d["seconds"] = divmod(rem, 60)
    return f"{d['hours']:02d}h:{d['minutes']:02d}m:{d['seconds']:02}s"

def psutil_search_by_name(name:str):
    for proc_name in psutil.process_iter():
        if name.lower() in proc_name.name().lower():
            return [True]
    return [False]

def psutil_kill_by_name(name:str):
    proc_name: psutil.Process
    for proc_name in psutil.process_iter():
        if name.lower() in proc_name.name().lower():
            _parent = proc_name.parent()
            if _parent == None:
                proc_name.kill()
            else:
                _parent.kill()
            return [True]
    return [False]

def read_config(path: str) -> List[str]:
    # game log -> 0 , app log -> 1
    _default_save_path = ['','']
    with open('config.cfg','r') as f:
        lines = f.readlines()
        for line in lines:
            if line[0] == '#':
                continue
            else:
                line = line.replace('\n','').replace("'",'').replace('"','')
                line = line.split('=')
                if line[0] == 'default_game_log_save':
                    _default_save_path[0] = line[1]
                else:
                    _default_save_path[1] = line[1]

    return _default_save_path

class gameMonitor(QObject):
    finished = pyqtSignal()
    # progress = pyqtSignal(list) # start, session, status,
    progress = pyqtSignal(int, str)

    def __init__(self, payload):
        super().__init__()
        self.continue_run = True
        self.payload = payload

    def run(self): # game to monitor, start, session, status
        game_to_monitor, start_time, session, status = self.payload

        while True:
            for game_name in game_to_monitor.keys():
                if not game_to_monitor[game_name]:
                    continue
                else:
                    _tmp = psutil_search_by_name(game_name)
                    _tmp = sum(_tmp)
                    if _tmp > 0 and status[game_name] is False:
                        session[game_name] += 1
                        start_time[game_name] = datetime.datetime.now()
                        status[game_name] = True
                        self.progress.emit(0, game_name) # start
                        # self.progress.emit([start_time, session, status])
                    elif _tmp <= 0 and status[game_name] is True:
                        status[game_name] = False
                        self.progress.emit(1, game_name) # stop
                        # self.progress.emit([start_time, session, status])
                    elif _tmp > 0 and status[game_name] is True:
                        self.progress.emit(2, game_name) # keep playing
                        # self.progress.emit([start_time, session, status])
                    else:
                        pass
                        # self.progress.emit(3, game_name) # not running
                        # self.progress.emit([start_time, session, status])
            # self.progress.emit([start_time, session, status])
            for x in range(30):
                QThread.sleep(1)
                if not self.continue_run: 
                    self.finished.emit()
                    return

    def stop(self):
        self.continue_run = False

class appMonitor(QObject):
    finished = pyqtSignal()
    detected_signal = pyqtSignal(str)

    def __init__(self, payload:dict, rate:int):
        super().__init__()
        self.payload = payload
        self.continue_run = True
        self.update_rate = rate

    def run(self):
        _app_to_monitor = self.payload
        while True:
            for x in _app_to_monitor.keys():
                if not _app_to_monitor[x]:
                    continue
                else:
                    _tmp = psutil_kill_by_name(x)
                    _tmp = sum(_tmp)
                    if _tmp > 0:
                        self.detected_signal.emit(x)

            for x in range(self.update_rate):
                QThread.sleep(1)
                if not self.continue_run: 
                    self.finished.emit()
                    return
        
    def stop(self):
        self.continue_run = False

class worktimeLogger(QObject):
    finished = pyqtSignal()
    # pause_resume_signal = pyqtSignal(str)
    update_signal = pyqtSignal(datetime.timedelta, bool)
    

    def __init__(self, save_path):
        super().__init__()
        self.continue_run = True
        self.pause_run = False
        self.time_start = datetime.datetime.now()
        self.total_work_duration = datetime.timedelta() # including after break
        self.time_break = None

        self.save_path = os.path.join(save_path,'working_time.txt')

    def run(self):
        _open_mode = 'a' if os.path.isfile(self.save_path) is True else 'w'
        with open(self.save_path,_open_mode) as f:
            f.write(f"start {self.time_start.strftime('%y-%b-%d %H:%M:%S')}\n")
        self.work_last_update = self.time_start
        self._work_duration = None
        while True:
            if not self.pause_run:
                # _time_delta = datetime.datetime.now() - self.time_start
                # self.time_start += _time_delta
                self._work_duration = datetime.datetime.now() - self.time_start
                self.update_signal.emit(self.total_work_duration+self._work_duration, self.pause_run)
            else:
                _time_delta = datetime.datetime.now() - self.time_break
                self.update_signal.emit(_time_delta, self.pause_run)
            # for x in range(10):
            QThread.sleep(1)
            if not self.continue_run:
                self.log_end()
                return

    def log_end(self):
        with open(self.save_path, 'a') as f:
            f.write(f"stop {(datetime.datetime.now()).strftime('%y-%b-%d %H:%M:%S')}\n")
        self.finished.emit()

    def resume(self):
        self.pause_run = False
        self.time_start = datetime.datetime.now()
        with open(self.save_path,'a') as f:
            f.write(f"resume {self.time_start.strftime('%y-%b-%d %H:%M:%S')}\n")

    def pause_resume(self, signal):
        if signal.lower() == 'pause':
            self.pause_run = True
            _open_mode = 'a' if os.path.isfile(self.save_path) is True else 'w'
            self.time_break = datetime.datetime.now()
            with open(self.save_path,_open_mode) as f:
                f.write(f"break {self.time_break.strftime('%y-%b-%d %H:%M:%S')}\n")
            self.total_work_duration += self._work_duration
            # self.pause_signal.emit()
        elif signal.lower() == 'resume':
            self.resume()

    def stop(self):
        self.continue_run = False

class mainWindow(QMainWindow):

    # signaling with pyqtSignal
    stop_sigal_game = pyqtSignal()
    stop_signal_app = pyqtSignal()
    stop_sigal_work = pyqtSignal()
    pause_resume_signal_work = pyqtSignal(str)

    def __init__(self):
        super().__init__()
        # game log path -> 0 ; work log path -> 1
        _default_path = read_config('config.cfg')
        # app stuff
        # self.default_save_path = _default_path[0]
        # self.start_game = {'Genshin':None, 'Dota2':None} # datetime object
        # self.session_game = {'Genshin':0, 'Dota2':0}
        # self.status_game = {'Genshin':False, 'Dota2':False}
        # self.monitor_game = {'Genshin':True, 'Dota2':False}
        # self.status_app = {'Discord':False}
        # self.monitor_app = {'Discrod':False}

        # tray related notif
        self.tray_notif = False

        # game related
        self.start_game = {}
        self.session_game = {}
        self.status_game = {}
        self.monitor_game = {}

        # app related
        self.status_app = {}
        self.monitor_app = {}

        # working time monitor related
        self.working_monitor_app_running = False

        # UI
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)     
        # set default save path
        self.ui.editSavePath.setText(_default_path[0])
        self.ui.editSavePathWorkingTime.setText(_default_path[1])
        
        # get checkbox game and set default status
        self.checkbox_game = []
        for x in range(self.ui.gridLayout_gameCheckBox.count()):
            _tmp = self.ui.gridLayout_gameCheckBox.itemAt(x).widget()
            if isinstance(_tmp, QCheckBox):
                self.checkbox_game.append(_tmp)
                self.start_game[_tmp.text()] = None
                self.session_game[_tmp.text()] = 0
                self.status_game[_tmp.text()] = False
                self.monitor_game[_tmp.text()] = False

        # get checkbox app and set default
        self.checkbox_app = []
        for x in range(self.ui.gridLayout_appToBlock.count()):
            _tmp = self.ui.gridLayout_appToBlock.itemAt(x).widget()
            if isinstance(_tmp, QCheckBox):
                self.checkbox_app.append(_tmp)
                self.status_app[_tmp.text()] = False
                self.monitor_app[_tmp.text()] = False

        self.set_connection()
        # button default state
        self.ui.stopButton.setEnabled(False)
        self.ui.stopBlock.setEnabled(False)
        self.ui.stopWorkLogger.setEnabled(False)
        self.ui.pauseWorkLogger.setEnabled(False)
        self.ui.lineEdit_appMonitoringRefreshRate.setText('30')

        # text box default state
        self.ui.textData.setReadOnly(True)
        self.ui.textDataBlockApp.setReadOnly(True)
        self.ui.lineEdit_workingTimeInfo.setReadOnly(True)

        # # set game and app status
        

        # timer
        self.timer_game_monitor = QtCore.QTimer(self, interval=5000, timeout=self.game_monitor)

        # self.start_pressed_threading()

    def set_connection(self):
        #TODO runButton -> gameLogRunButton
        # game monitor
        self.ui.runButton.clicked.connect(self.start_pressed_threading)
        self.ui.stopButton.clicked.connect(self.stop_pressed_threading)
        # app monitor
        self.ui.runBlock.clicked.connect(self.start_pressed_threading_app)
        self.ui.stopBlock.clicked.connect(self.stop_pressed_threading_app)
        # work time logger
        self.ui.runWorkLogger.clicked.connect(self.start_working_time_monitor)
        self.ui.stopWorkLogger.clicked.connect(self.end_working_time_monitor)
        self.ui.pauseWorkLogger.clicked.connect(self.pause_resume_working_time_monitor)

    ####################### windows resizing event catcher
    def changeEvent(self, event):
        if event.type() == QEvent.WindowStateChange:
            if self.windowState() and Qt.WindowMinimized:
                self.tray_system = QSystemTrayIcon()
                self.tray_system.setIcon(QIcon('icon/favicon.ico'))
                self.tray_system.show()
                if not self.tray_notif:
                    self.tray_system.showMessage('Notification','Minimized to tray icon')
                    self.tray_notif = True
                self.tray_system.activated.connect(self.trayIconCatcher)
                self.hide()

    def trayIconCatcher(self, reason):
        if reason == 2:
            self.tray_system.hide()
            self.show()
            self.showNormal()
    ####################### windows resizing event catcher    

    ####################### app closing event catcher
    def closeEvent(self, event: QCloseEvent):
        if self.working_monitor_app_running:
            with open(os.path.join(self.ui.editSavePathWorkingTime.text(),'working_time.txt'),'a') as f:
                f.write(f"stop {(datetime.datetime.now()).strftime('%y-%b-%d %H:%M:%S')}\n")
            
            event.accept()
        else:
            event.accept()
                

    ####################### app closing event catcher


    ####################### app monitor
    def start_pressed_threading_app(self):
        self.ui.runBlock.setEnabled(False)
        self.ui.stopBlock.setEnabled(True)
        self.ui.lineEdit_appMonitoringRefreshRate.setReadOnly(True)

        # get what to monitor and deactivate the checkbox
        for checkbox in self.checkbox_app:
            self.monitor_app[checkbox.text()] = checkbox.isChecked()
            checkbox.setEnabled(False)

        _app_to_monitor = [x for x in self.monitor_app.keys() if self.monitor_app[x]]

        if len(_app_to_monitor) > 1: 
            self.ui.textDataBlockApp.appendPlainText(f'Preventing {" and ".join(_app_to_monitor)} from running.')
        elif len(_app_to_monitor) == 1 :
            self.ui.textDataBlockApp.appendPlainText(f"Preventing {_app_to_monitor[0]} from running.")
        else:
            self.ui.textDataBlockApp.appendPlainText('Please make sure at least one app is checked.')
            self.button_handler_finish_app()
            return
        
        self.thread_app = QThread()
        self.worker_monitor_app = appMonitor(self.monitor_app, int(self.ui.lineEdit_appMonitoringRefreshRate.text()))
        self.stop_signal_app.connect(self.worker_monitor_app.stop)
        
        self.worker_monitor_app.detected_signal.connect(self.app_detected_reporting)
        self.worker_monitor_app.finished.connect(self.thread_app.quit)
        self.worker_monitor_app.finished.connect(self.worker_monitor_app.deleteLater)
        self.worker_monitor_app.moveToThread(self.thread_app)

        # thread signaling
        self.thread_app.started.connect(self.worker_monitor_app.run)
        self.thread_app.finished.connect(self.thread_app.deleteLater)
        self.thread_app.finished.connect(self.worker_monitor_app.stop)
        self.thread_app.finished.connect(self.button_handler_finish_app)

        self.thread_app.start()

    def app_detected_reporting(self, payload):
        # payload -> app name
        self.ui.textDataBlockApp.appendPlainText(f"{payload} app detected. Tried killing the app.")

    def stop_pressed_threading_app(self):
        self.stop_signal_app.emit()
        self.ui.textDataBlockApp.appendPlainText('Finished monitoring.')

    def button_handler_finish_app(self):
        self.ui.runBlock.setEnabled(True)
        self.ui.stopBlock.setEnabled(False)
        self.ui.lineEdit_appMonitoringRefreshRate.setReadOnly(False)
        for checkbox in self.checkbox_app:
            checkbox.setEnabled(True)
    ####################### app monitor


    ####################### game monitor
    def start_pressed_threading(self):
        self.ui.runButton.setEnabled(False)
        self.ui.stopButton.setEnabled(True)
        self.ui.editSavePath.setReadOnly(True)
        # get checked item
        for checkbox in self.checkbox_game:
            self.monitor_game[checkbox.text()] = checkbox.isChecked()
            checkbox.setEnabled(False)

        to_log = [x for x in self.monitor_game.keys() if self.monitor_game[x]]
        if len(to_log) > 1:
            self.ui.textData.appendPlainText('Logging started')
            self.ui.textData.appendPlainText(f"Monitoring {' and '.join(to_log)}")
        elif len(to_log) == 1:
            self.ui.textData.appendPlainText('Logging started')
            self.ui.textData.appendPlainText(f"Monitoring {to_log[0]}")
        else:
            self.ui.textData.appendPlainText('Please make sure at least one game is checked.')
            self.ui.runButton.setEnabled(True)
            self.ui.stopButton.setEnabled(False)
            for checkbox in self.checkbox_game:
                checkbox.setEnabled(True)
            return

        self.thread_game = QThread()
        payload = [self.monitor_game, self.start_game, self.session_game, self.status_game]
        self.gameLogger = gameMonitor(payload)
        self.stop_sigal_game.connect(self.gameLogger.stop) # must be before moving to thread
        
        # signaling of gameLogger
        self.gameLogger.progress.connect(self.gameLoggerProgress)
        self.gameLogger.finished.connect(self.thread_game.quit)
        self.gameLogger.finished.connect(self.gameLogger.deleteLater)
        self.gameLogger.moveToThread(self.thread_game)

        # signaling of thread
        self.thread_game.started.connect(self.gameLogger.run)
        self.thread_game.finished.connect(self.thread_game.deleteLater)
        self.thread_game.finished.connect(self.gameLogger.stop)
        self.thread_game.finished.connect(self.buttonHandler)

        # start thread
        self.thread_game.start()
    
    def buttonHandler(self):
        # make sure all button change happen affter thread is killed
        for x in self.checkbox_game:
            x.setEnabled(True)
        self.ui.runButton.setEnabled(True)
        self.ui.stopButton.setEnabled(False)
        self.ui.editSavePath.setReadOnly(False)

    def stop_pressed_threading(self):
        self.ui.stopButton.setEnabled(False)
        self.stop_sigal_game.emit()
        self.ui.textData.appendPlainText('Logging stopped. Killing thread')
    ####################### game monitor

    ####################### working time monitor
    def start_working_time_monitor(self):
        self.ui.runWorkLogger.setEnabled(False)
        self.ui.stopWorkLogger.setEnabled(True)
        self.ui.pauseWorkLogger.setEnabled(True)
        self.ui.editSavePathWorkingTime.setReadOnly(True)
        self.ui.lineEdit_workingTimeInfo.setStyleSheet('color: green')

        

        self.thread_work_log = QThread()
        self.workLogger = worktimeLogger(self.ui.editSavePathWorkingTime.text())
        self.stop_sigal_work.connect(self.workLogger.stop)

        # signaling of workLogger
        self.workLogger.finished.connect(self.workButtonHandler)
        self.workLogger.finished.connect(self.workLogger.deleteLater)
        self.workLogger.finished.connect(self.thread_work_log.quit)
        self.workLogger.update_signal.connect(self.work_time_update)
        self.pause_resume_signal_work.connect(self.workLogger.pause_resume)
        self.workLogger.moveToThread(self.thread_work_log)

        # signaling of thread
        self.thread_work_log.started.connect(self.workLogger.run)
        self.thread_work_log.finished.connect(self.thread_work_log.deleteLater)
        self.thread_work_log.finished.connect(self.workLogger.stop)
        self.thread_work_log.finished.connect(self.workButtonHandler)

        self.thread_work_log.start()

        self.working_monitor_app_running = True

    def workButtonHandler(self):
        # final reset
        self.ui.runWorkLogger.setEnabled(True)
        self.ui.stopWorkLogger.setEnabled(False)
        self.ui.pauseWorkLogger.setEnabled(False)
        self.ui.editSavePathWorkingTime.setReadOnly(False)
        self.ui.lineEdit_workingTimeInfo.setStyleSheet('color:black')
        self.working_monitor_app_running = False

    def work_time_update(self, time:datetime.timedelta, state:bool):
        _stringdelta = strfdelta(time)
        self.ui.lineEdit_workingTimeInfo.setText(_stringdelta)
        if not state:
            self.ui.lineEdit_workingTimeInfo.setStyleSheet('color:green')
        else:
            self.ui.lineEdit_workingTimeInfo.setStyleSheet('color:blue')

    def pause_resume_working_time_monitor(self):
        _text = self.ui.pauseWorkLogger.text()
        self.pause_resume_signal_work.emit(_text)
        if _text == 'Pause':
            self.ui.pauseWorkLogger.setText('Resume')
        else:
            self.ui.pauseWorkLogger.setText('Pause')

    def end_working_time_monitor(self):
        self.stop_sigal_work.emit()

    ####################### working time monitor

    ####################### non-threading things
    def start_pressed(self):
        self.ui.runButton.setEnabled(False)
        self.ui.stopButton.setEnabled(True)
        # self.counter = 0
        # QtCore.QTimer.singleShot(0, self.textbomb)
        to_log = [x for x in self.monitor_game.keys() if self.monitor_game[x]]
        self.ui.textData.appendPlainText('Logging started')
        if len(to_log) > 1:
            self.ui.textData.appendPlainText(f"Monitoring {' and '.join(to_log)}")
        else:
            self.ui.textData.appendPlainText(f"Monitoring {to_log[0]}")
        #TODO add info what game to log
        self.timer_game_monitor.start()

    def stop_pressed(self):
        self.ui.runButton.setEnabled(True)
        self.ui.stopButton.setEnabled(False)
        self.timer_game_monitor.stop()
        self.ui.textData.appendPlainText('Logging stopped')
    
    # game logger progress with threading
    def gameLoggerProgress(self, payload: int, game_name: str):
        if payload == 0:
            file_exist = os.path.isfile(os.path.join(self.ui.editSavePath.text(), f'play_time_{game_name}.txt'))
            with open(os.path.join(self.ui.editSavePath.text(), f'play_time_{game_name}.txt'), 'a' if file_exist else 'w') as f:
                self.ui.textData.appendPlainText(f'{game_name} found! Logging of session {self.session_game[game_name]} started')
                f.write('{} {}\n'.format('start', self.start_game[game_name].strftime('%y-%b-%d %H:%M:%S')))

        elif payload == 1:
            file_exist = os.path.isfile(os.path.join(self.ui.editSavePath.text(), f'play_time_{game_name}.txt'))
            with open(os.path.join(self.ui.editSavePath.text(), f'play_time_{game_name}.txt'), 'a' if file_exist else 'w') as f:
                self.ui.textData.appendPlainText(f'{game_name} ended! Session {self.session_game[game_name]} logging finished')
                end_time = datetime.datetime.now()
                f.write('{} {}\n'.format('end', end_time.strftime('%y-%b-%d %H:%M:%S')))
                self.ui.textData.appendPlainText(f"You played {game_name} for {(end_time - self.start_game[game_name])}")
                self.status_game[game_name] = False
                self.start_game[game_name] = None

        elif payload == 2:
            self.ui.textData.appendPlainText(f"Playing {game_name}. Session {self.session_game[game_name]} for {(datetime.datetime.now()-self.start_game[game_name])}")

        else:
            self.ui.textData.appendPlainText(f"{game_name} is not running")

    def game_monitor(self): # non-threading
        all_process = [process.name() for process in psutil.process_iter()]
        for game_name in self.monitor_game.keys():
            if not self.monitor_game[game_name]:
                continue
            else:
                _tmp = [True if game_name in x else False for x in all_process]
                _tmp = sum(_tmp)
                if _tmp > 0 and self.status_game[game_name] is False:
                    file_exist = os.path.isfile(os.path.join(self.ui.editSavePath.text(), f'play_time_{game_name}.txt'))
                    with open(os.path.join(self.ui.editSavePath.text(), f'play_time_{game_name}.txt'), 'a' if file_exist else 'w') as f:
                        self.session_game[game_name] += 1
                        self.ui.textData.appendPlainText(f'{game_name} found! Logging of session {self.session_game[game_name]} started')
                        self.start_game[game_name] = datetime.datetime.now()
                        f.write('{} {}\n'.format('start', self.start_game[game_name].strftime('%y-%b-%d %H:%M:%S')))
                        self.status_game[game_name] = True
                
                elif _tmp <= 0 and self.status_game[game_name] is True:
                    file_exist = os.path.isfile(os.path.join(self.ui.editSavePath.text(), f'play_time_{game_name}.txt'))
                    with open(os.path.join(self.ui.editSavePath.text(), f'play_time_{game_name}.txt'), 'a' if file_exist else 'w') as f:
                        self.ui.textData.appendPlainText(f'{game_name} ended! Session {self.session_game[game_name]} logging finished')
                        end_time = datetime.datetime.now()
                        f.write('{} {}\n'.format('end', end_time.strftime('%y-%b-%d %H:%M:%S')))
                        self.ui.textData.appendPlainText(f"You play {game_name} for {(end_time - self.start_game[game_name])}")
                        self.status_game[game_name] = False
                elif _tmp > 0 and self.status_game[game_name] is True:
                    self.ui.textData.appendPlainText(f"Playing {game_name}. Session {self.session_game[game_name]} for {(datetime.datetime.now()-self.start_game[game_name])}")
                else:
                    self.ui.textData.appendPlainText(f"Monitored game is not running")

    def textbomb(self):
        self.ui.textData.appendPlainText('{}'.format(self.counter))
        self.counter += 1


if __name__ == '__main__':
    app = QApplication(sys.argv)
    app.setWindowIcon(QIcon('icon/favicon.ico'))
    win = mainWindow()
    win.show()
    sys.exit(app.exec())
