import os
import datetime
import matplotlib
from matplotlib import font_manager
import matplotlib.pyplot as plt
import matplotlib.colors as mcolors
import numpy as np
from pdb import set_trace as bp
from scipy.interpolate import make_interp_spline, BSpline

####### PATCH DATE ######
patch_13 = datetime.datetime.strftime(datetime.datetime(year=2021, month=3, day=7),'%y-%b-%d')
patch_14 = datetime.datetime.strftime(datetime.datetime(year=2021, month=3, day=17),'%y-%b-%d')
patch_15 = datetime.datetime.strftime(datetime.datetime(year=2021, month=4, day=28),'%y-%b-%d')
patch_16 = datetime.datetime.strftime(datetime.datetime(year=2021, month=6, day=9),'%y-%b-%d')
patch_20 = datetime.datetime.strftime(datetime.datetime(year=2021, month=7, day=21),'%y-%b-%d')


###### FONT
font_dirs = ['\\fonts']
font_files = font_manager.findSystemFonts(fontpaths=font_dirs)
for font_file in font_files:
    font_manager.FontManager.addfont(font_file)
matplotlib.rc('font', family='Hind Siliguri')

to_parse = 'E:\Self Related Stuff\iseng\process_list\play_time.txt'

base = datetime.datetime(year=2021, month=3, day=7)

play_time = {}
while True:
    play_time[datetime.datetime.strftime(base,'%y-%b-%d')] = datetime.timedelta(seconds=0)
    base += datetime.timedelta(days=1)
    if (datetime.datetime.today() - base).days < 0:
        break

with open(to_parse, 'r') as f:
    data = f.readlines()

for idx, _line in enumerate(data):
    _line = _line.replace('\n','').split(' ')
    if _line[0] == 'end':
        continue
    if _line[0] == 'start':
        start_time = datetime.datetime.strptime(_line[1]+' '+_line[2], '%y-%b-%d %H:%M:%S')
        _line_data_end = data[idx+1].replace('\n','').split(' ')
        if _line_data_end[0] != 'end':
            continue
        end_time = datetime.datetime.strptime(_line_data_end[1]+' '+_line_data_end[2], '%y-%b-%d %H:%M:%S')
        total_time = end_time - start_time
        dict_key = datetime.datetime.strftime(start_time,'%y-%b-%d')
        try:
            play_time[dict_key] += total_time
        except KeyError:
            play_time[dict_key] = total_time

keys = list(play_time.keys())
fig, ax = plt.subplots(1,1, figsize=(16,5), dpi=300)
data = [(play_time[x].seconds/60.0) for x in keys]
new_x_axis = np.linspace(0, len(data)-1, 1000)
spl = make_interp_spline(np.arange(0,len(data)), data, k=13)
data_smooth = spl(new_x_axis)
# ax.plot(data_smooth)
ax.bar(np.arange(0,len(data)),data,color='#de9873')
# ax.legend(['Weekday'])
# weekend
for key in keys:
    date_time = datetime.datetime.strptime(key,'%y-%b-%d')
    if date_time.weekday() == 5 or date_time.weekday() == 6:
        ax.bar(keys.index(key),data[keys.index(key)],color='#293241')
ax.legend(['Weekdays','Weekends'])
ax.set_title('Genshin Daily Play Time', fontsize=16)
# ax.set_facecolor('#eaeaf2')
ax.set_xlim(-0.5,len(keys)-0.5)
ax.set_ylim(0,350)
ax.set_ylabel('Minutes', fontsize=12)
ax.set_xlabel('Date', fontsize=12)
ax.set_xticks(np.linspace(0,len(data)-1,15,dtype=int))
ax.set_xticklabels(np.array(keys)[np.linspace(0,len(data)-1,15,dtype=int)])

alpha = 0.5
# patch 13
idx_start = -1
idx_end = keys.index(patch_14)
# ax.plot(np.arange(idx_start,idx_end+1),[300]*len(np.arange(idx_start,idx_end+1)), color='#96b5bc')
ax.fill_between(np.arange(idx_start,idx_end+1), [350]*len(np.arange(idx_start,idx_end+1)), color='#96b5bc', alpha=alpha)

# patch 14
idx_start = keys.index(patch_14)
idx_end = keys.index(patch_15)
# ax.plot(np.arange(idx_start,idx_end+1),[350]*len(np.arange(idx_start,idx_end+1)),color='#dee3d6')
ax.fill_between(np.arange(idx_start,idx_end+1), [350]*len(np.arange(idx_start,idx_end+1)), color='#dee3d6', alpha=alpha)

# patch 15
idx_start = keys.index(patch_15)
idx_end = keys.index(patch_16)
# ax.plot(np.arange(idx_start,idx_end+1),[350]*len(np.arange(idx_start,idx_end+1)),color='#eeb169')
ax.fill_between(np.arange(idx_start,idx_end+1), [350]*len(np.arange(idx_start,idx_end+1)), color='#f2cb64', alpha=alpha)

# patch 16
idx_start = keys.index(patch_16)
idx_end = keys.index(patch_20)
# ax.plot(np.arange(idx_start,idx_end+1),[350]*len(np.arange(idx_start,idx_end+1)),color='#a0d3ff')
ax.fill_between(np.arange(idx_start,idx_end+1), [350]*len(np.arange(idx_start,idx_end+1)), color='#a0d3ff', alpha=alpha)

# patch 20
idx_start = keys.index(patch_20)
idx_end = len(keys)
# ax.plot(np.arange(idx_start,idx_end+1),[350]*len(np.arange(idx_start,idx_end+1)),color='#9b8bc5')
ax.fill_between(np.arange(idx_start,idx_end+1), [350]*len(np.arange(idx_start,idx_end+1)), color='#9b8bc5', alpha=alpha)

plt.savefig('{}.png'.format('total time - '+datetime.datetime.now().strftime('%Y-%m-%d')), facecolor=fig.get_facecolor())

data = np.array(data)
print('average: {}, median: {}, max: {}'.format(data.mean(), np.median(data), data.max()))
# bp()
# plt.show()
# bp()